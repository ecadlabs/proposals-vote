LIGO := scripts/ligo
CLIENT := /snap/bin/tezos-mainnet.client
duration=864000

compile:
	($(LIGO) compile-contract src/contracts/mono_vote.ligo main) > mono_vote.tz
	sed -i.bak 's/\r$$//' mono_vote.tz

test:
	pytest tests/test_mono.py

clean:
	rm -f *.tz *.pp.ligo *.bak

# now returns january 1st 1970
init_arg=$(shell $(LIGO) compile-storage src/contracts/mono_vote.ligo main "record proposal = ( \"great feature\" : hash ); voters = (map end : voters); votes = (map 1n -> 0n; 2n -> 0n; 3n -> 0n; end : map(nat,nat)); enddate = ( now : timestamp); admins = ( set (\"tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx\" : address) end : set(address) ) end")

originate: compile
	echo "$(init_arg)"
	$(CLIENT) originate contract mono transferring 0 \
		from bootstrap1 running mono_vote.tz \
		--init '$(init_arg)' --burn-cap 5 --force

init_client:
	$(CLIENT) import secret key bootstrap1 unencrypted:edsk3gUfUPyBSfrS9CCgmCiQsTCHGkviBDusMxDJstFtojtc1zcpsh
	$(CLIENT) import secret key bootstrap2 unencrypted:edsk39qAm1fiMjgmPkw1EgQYkMzkJezLNewd7PLNHTkr6w9XA2zdfo
	$(CLIENT) import secret key bootstrap3 unencrypted:edsk4ArLQgBTLWG5FJmnGnT689VKoqhXwmDPBuGx3z4cvwU9MmrPZZ
	$(CLIENT) import secret key bootstrap4 unencrypted:edsk2uqQB9AY4FvioK2YMdfmyMrer5R8mGFyuaLLFfSRo8EoyNdht3
	$(CLIENT) import secret key bootstrap5 unencrypted:edsk4QLrcijEffxV31gGdN2HU7UpyJjA8drFoNcmnB28n89YjPNRFm

transfer:
	$(CLIENT) transfer 0 from bootstrap1 to $(addr) --arg '$(arg)' --entrypoint "vote"

add_voters:
	$(CLIENT) transfer 0 from bootstrap1 to mono --arg '(Left (Left (Right { Elt "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" 10 ; Elt "tz1b7tUupMgCNw2cCLpKTkSD1NZzB5TkP2sv" 12 ; Elt "tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv" 13 ; Elt "tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN" 11 })))' --burn-cap 0.124
	$(CLIENT) transfer 0 from bootstrap1 to mono --arg '(Right (Left $(duration)))' --burn-cap 0.124

	# python3 ./scripts/gen_voters.py --client "$(CLIENT)" --jsonfile "tests/voters.json" --addr "mono" --duration 864000
