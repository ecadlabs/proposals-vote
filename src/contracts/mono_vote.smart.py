import smartpy as sp

class Vote(sp.Contract):
    def __init__(self):
      voter = sp.map({sp.address("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"): sp.nat(123)})
      votes = sp.map({1 : 0, 2: 0, 3: 0})
      proposal = "hello i'm a hash"
      enddate = sp.timestamp(1571761674)
      admins = sp.set()
      self.init(voters = voter, votes=votes, proposal = proposal, enddate = enddate, admins = admins)
    
    @sp.entryPoint
    def vote(self, vote):
      sp.verify(sp.now < self.data.enddate)
      self.data.votes[vote] += self.data.voters[sp.sender]
      del self.data.voters[sp.sender]

    @sp.entryPoint
    def addAdmin(self, addr):
      sp.verify(self.data.admins.contains(sp.sender))
      self.data.admins.add(addr)

    @sp.entryPoint
    def remAdmin(self, addr):
      sp.verify(self.data.admins.contains(sp.sender))
      self.data.admins.remove(addr)

    @sp.entryPoint
    def reinit(self, params):
      sp.verify(self.data.admins.contains(sp.sender))
      sp.verify(sp.now > self.data.enddate)
      self.data.voters = params.voters
      self.data.enddate = params.enddate
      self.data.proposal = params.proposal
      self.data.votes = sp.map({1 : 0, 2: 0, 3: 0})


@addTest(name = "vote")
def test():
  scenario = sp.testScenario()
  vote = Vote()
  scenario += vote
  scenario += vote.vote(1).run(sender = "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx")
  scenario += vote.vote(1).run(sender = "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx", valid = False)
  scenario.verify(vote.data.votes[1] == 123)
