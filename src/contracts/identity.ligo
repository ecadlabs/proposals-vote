
// Indentity contract
// Store a map of addresses -> rolls
// Approve a ballot only if the address is in the identity map
// rolls information is stored

// TODO:
// add lock/unlock . Vote actions are valid only if the contract is locked

type vote is nat
type ballot is (address * list(vote))
type rolls is int
type identity is address * rolls

type storage is record
  identities : map(address,rolls);
  admins: set(address);
end

type action is
  | RemoveAdmin of address
  | AddAdmin of address
  | AddVoters of list(identity)
  | RemoveVoter of address
  | Approve of ballot

function fail_if_not_admin(const admins: set(address)): unit is
  block {
    if (not(set_mem(sender, admins))) then
      failwith("You don't have this privilege.")
    else skip
  } with unit

function main(const action: action; const storage: storage) : (list(operation) * storage) is
  begin
    fail_if_not_admin(storage.admins);
    const operations : list(operation) = nil;
    case action of
    | AddAdmin(addr) -> begin
        storage.admins := set_add(addr, storage.admins)
      end
    | RemoveAdmin(addr) -> begin
        storage.admins := set_remove(addr, storage.admins)
      end
    | AddVoters(addrlist) -> begin
        function add(const el : identity) : unit is
          block { storage.identities[el.0] := el.1 }
        with unit;
        list_iter(add, addrlist)
      end
    | RemoveVoter(addr) -> begin
        remove addr from map storage.identities
      end
    | Approve(ballot) -> begin
        // source here is the calling contract that must be listed as admin
        // TODO separate calling contract addresses from admin addresses
        if (map_mem(ballot.0, storage.identities)) then begin
          const params : ballot = ballot;
          const receiver : contract(ballot) = get_contract(source);
          const approve_operation : operation = transaction(params, 0mutez, receiver);
          operations := list approve_operation end;
        end else
          failwith("This address is not in the identity set")
      end
    end;
  end with ((operations : list(operation)), storage)
