type hash is string;

type parameter is int;

// We're doing dirty enums here
// 0 -> Abstain
// 1 -> Yay
// 2 -> Nay
// 3 -> Pass

type voters is map(address, nat)

type storage is record
  proposal: hash;
  voters: voters;
  votes: map(nat, nat);
  enddate: timestamp;
  admins: set(address);
end

type action is
  | Vote of nat
  | StartVote of int
  | Init of hash
  | RemoveAdmin of address
  | AddAdmin of address
  | AddVoters of map(address, nat)

function fail_if_not_admin(const admins: set(address)): unit is
  block {
    if (not(set_mem(sender, admins))) then
      failwith("You don't have this privilege.")
    else skip
  } with unit

function fail_if_ongoing(const enddate: timestamp): unit is
  block {
    if (now < enddate) then
      failwith("The vote is still ongoing.")
    else skip
  } with Unit

function fail_if_ended(const enddate: timestamp): unit is
  block {
    if (now > enddate) then
      failwith("The vote has ended.")
    else skip
  } with unit

function main(const action: action; const storage: storage) : (list(operation) * storage) is
  begin
    case action of
    | AddAdmin(addr) -> begin
        fail_if_not_admin(storage.admins);
        storage.admins := set_add(addr, storage.admins)
      end
    | RemoveAdmin(addr) -> begin
        fail_if_not_admin(storage.admins);
        storage.admins := set_remove(addr, storage.admins)
      end
    | AddVoters(voters) -> begin
        function add(const addr: address; const rolls: nat) : unit is
          block { storage.voters[addr] := rolls }
        with unit;
        map_iter(add, voters)
      end
    | StartVote(offset) -> begin
        const enddate : timestamp = now + offset;
        fail_if_ongoing(storage.enddate);
        storage.enddate := enddate
      end
    | Init(proposal) -> begin
        fail_if_not_admin(storage.admins);
        fail_if_ongoing(storage.enddate);
        patch storage with record [
          voters = map end;
          proposal = proposal;
          votes = map 1n -> 0n; 2n -> 0n; 3n -> 0n; end
        ]
      end
    | Vote(vote) -> begin
        fail_if_ended(storage.enddate);
        const rolls: nat = get_force(sender, storage.voters);
        storage.votes[vote] := get_force(vote, storage.votes) + rolls;
        remove sender from map storage.voters;
      end
    end
  end with ((nil : list(operation)), storage)
