
// Simple contract to store vote ballots
// (TODO) The contract ask for a deposit of 5 Tez that will be refunded is
// the person voting is authorized to do so. Otherwise the vote won't be
// counted and the deposit kept.
// This contract calls a second contract holding the list of authorized voters

// using a map address -> vote the user can vote multiple time and change its mind

// since the vote is a list of nats, a simple yes, nay vote would be represented
// with a list with a singleton, while a preferential vote as a list of preferences

type vote is list(nat)
type hash is string

type ballot is (address * vote)

type storage is record
  proposal: hash;
  expires: timestamp;
  ballots: map(address,vote);
  identity: address;
end

type action is
  | Vote of vote
  | Approve of ballot

// this can be configured at init time defining the number of options
function check_vote (const l: vote) : bool is
  begin
    function check (const prec: bool; const cur: nat) : bool is
      begin skip end
    with ((cur > 0n) and (cur <= 4n) and prec)
  end with list_fold(check, l, True)

function fail_if_not_admin(const admins: set(address)): unit is
  block {
    if (not(set_mem(sender, admins))) then
      failwith("You don't have this privilege.")
    else skip
  } with unit

function main(const action: action; const storage: storage) : (list(operation) * storage) is
  begin
    const operations : list(operation) = nil;
    case action of
    | Vote(vote) -> begin
        // Everybody can call this contract to vote
        // TODO : add antispam 5 tez deposit
        if (check_vote(vote)) then begin
          const params : ballot = (sender, vote);
          const receiver : contract(ballot) = get_contract(storage.identity);
          const approve_operation : operation = transaction(params, 0mutez, receiver);
          operations := list approve_operation end
        end else
          failwith("Vote not conform with the available options")
      end
    | Approve(ballot) -> begin
        fail_if_not_admin(set storage.identity end);
        storage.ballots[ballot.0] := ballot.1
        // Refound the antispam deposit
        // const refound_deposit : operation = transaction(unit, 5mutez, addr);
        // operations = list refound_deposit end
      end
    end;
  end with ((operations : list(operation)), storage)
