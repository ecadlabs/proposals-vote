import pytest
import os
import json
import functools

cur_path = os.path.dirname(os.path.realpath(__file__))
path = os.path.dirname(cur_path) + '/'
contract = os.path.join(path,"src/contracts/", "mono_vote.ligo")
voter_json = os.path.join(path,"tests/voters.json")

def voters():
    data = json.load(open(voter_json,"r"))
    out = ""
    for r in data:
        out += f"(\"{r['phk']}\" : address ) -> {r['rolls']}n;";
    return out

@pytest.mark.ligo
class TestIdentity:
    """Test ."""

    def test_storage(self,ligo):
        params = """
            record
                proposal = ( "great feature" : hash );
                voters = (map end : voters);
                votes = (map 1n -> 0n; 2n -> 0n; 3n -> 0n; end : map(nat,nat));
                enddate = ( now + 432000 : timestamp);
                admins = ( set ("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" : address) end : set(address) )
            end"""
        out = ligo.compile_storage(
                [ contract, "main", f"{params}"])
        assert out['content'] == "(Pair (Pair (Pair { \"tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx\" } \"1970-01-06T00:00:01Z\")\n            (Pair \"great feature\" {}))\n      { Elt 1 0 ; Elt 2 0 ; Elt 3 0 })\n"

    def test_parameter(self,ligo):
        params = f"AddVoters( (map {voters()} end : map(address,nat)))"
        out = ligo.run(
                [ "compile-parameter", "--format", "json",
                    contract, "main", f"{params}"])
        assert json.loads(out)['content'] == "(Left (Left (Right\n               { Elt \"tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx\" 10 ;\n                 Elt \"tz1b7tUupMgCNw2cCLpKTkSD1NZzB5TkP2sv\" 12 ;\n                 Elt \"tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv\" 13 ;\n                 Elt \"tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN\" 11 })))\n"

    def test_AddAdmin(self, ligo):
        sender="tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"
        params = """AddAdmin(("tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN" : address ))"""
        storage = """
            record
                proposal = ( "great feature" : hash );
                voters = (map end : voters);
                votes = (map 1n -> 0n; 2n -> 0n; 3n -> 0n; end : map(nat,nat));
                enddate = ( now + 432000 : timestamp);
                admins = ( set ("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" : address) end : set(address) )
            end"""
        out = ligo.run_function(
                [ "--sender", sender,
                  contract, "main", f"( {params}, {storage} )"])
        assert out['content'] == "( [] , {votes = [ +1 -> +0 ; +2 -> +0 ; +3 -> +0 ] , voters = [] , proposal = \"great feature\" , enddate = +432001 , admins = { \n  address \"tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN\" ; address \"tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx\" }} )\n"

    def test_RemoveAdmin(self, ligo):
        sender="tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"
        params = """RemoveAdmin(("tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN" : address ))"""
        storage = """
            record
                proposal = ( "great feature" : hash );
                voters = (map end : voters);
                votes = (map 1n -> 0n; 2n -> 0n; 3n -> 0n; end : map(nat,nat));
                enddate = ( now + 432000 : timestamp);
                admins = ( set ("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" : address) end : set(address) )
            end"""
        out = ligo.run_function(
                [ "--sender", sender,
                  contract, "main", f"( {params}, {storage} )"])
        assert out['content'] == "( [] , {votes = [ +1 -> +0 ; +2 -> +0 ; +3 -> +0 ] , voters = [] , proposal = \"great feature\" , enddate = +432001 , admins = { \n  address \"tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx\" }} )\n"

    def test_AddVoters(self, ligo):
        sender="tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"
        params = f"AddVoters( (map {voters()} end : map(address,nat)))"
        storage = """
            record
                proposal = ( "great feature" : hash );
                voters = (map end : voters);
                votes = (map 1n -> 0n; 2n -> 0n; 3n -> 0n; end : map(nat,nat));
                enddate = ( now + 432000 : timestamp);
                admins = ( set ("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" : address) end : set(address) )
            end"""
        out = ligo.run_function(
                [ "--sender", sender,
                  contract, "main", f"( {params}, {storage} )"])
        assert out['content'] == "( [] , {votes = [ +1 -> +0 ; +2 -> +0 ; +3 -> +0 ] , voters = [ address \"tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx\" -> +10 ; address \"tz1b7tUupMgCNw2cCLpKTkSD1NZzB5TkP2sv\" -> +12 ; address \"tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv\" -> +13 ; address \"tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN\" -> +11 ] , proposal = \"great feature\" , enddate = +432001 , admins = { \n  address \"tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx\" }} )\n"

    def test_vote_empty(self, ligo):
        sender="tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"
        params = """Vote( 1n )"""
        storage = """
            record
                proposal = ( "great feature" : hash );
                voters = (map end : voters);
                votes = (map 1n -> 0n; 2n -> 0n; 3n -> 0n; end : map(nat,nat));
                enddate = ( now + 432000 : timestamp);
                admins = ( set ("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" : address) end : set(address) )
            end"""
        out = ligo.run_function(
                [ "--sender", sender,
                  contract, "main", f"( {params}, {storage} )"])
        assert out['status'] == "error"

    def test_vote_success(self, ligo):
        sender="tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"
        params = """Vote( 1n )"""
        storage = """
            record
                proposal = ( "great feature" : hash );
                voters = (map ("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" : address ) -> 10n end : voters);
                votes = (map 1n -> 0n; 2n -> 0n; 3n -> 0n; end : map(nat,nat));
                enddate = ( now + 432000 : timestamp);
                admins = ( set ("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" : address) end : set(address) )
            end"""
        out = ligo.run_function(
                [ "--sender", sender,
                  contract, "main", f"( {params}, {storage} )"])
        assert out['content'] == "( [] , {votes = [ +1 -> +10 ; +2 -> +0 ; +3 -> +0 ] , voters = [] , proposal = \"great feature\" , enddate = +432001 , admins = { \n  address \"tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx\" }} )\n"

    def test_vote_fail_wrong_vote(self, ligo):
        sender="tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"
        params = """Vote( 5n )"""
        storage = """
            record
                proposal = ( "great feature" : hash );
                voters = (map ("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" : address ) -> 10n end : voters);
                votes = (map 1n -> 0n; 2n -> 0n; 3n -> 0n; end : map(nat,nat));
                enddate = ( now + 432000 : timestamp);
                admins = ( set ("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" : address) end : set(address) )
            end"""
        out = ligo.run_function(
                [ "--sender", sender,
                  contract, "main", f"( {params}, {storage} )"])
        assert out['status'] == "error"

    def test_vote_fail_not_voter(self, ligo):
        sender="tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"
        params = """Vote( 1n )"""
        storage = """
            record
                proposal = ( "great feature" : hash );
                voters = (map ("tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN" : address ) -> 10n end : voters);
                votes = (map 1n -> 0n; 2n -> 0n; 3n -> 0n; end : map(nat,nat));
                enddate = ( now + 432000: timestamp);
                admins = ( set ("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" : address) end : set(address) )
            end"""
        out = ligo.run_function(
                [ "--sender", sender,
                  contract, "main", f"( {params}, {storage} )"])
        assert out['status'] == "error"

    def test_vote_fail_expired(self, ligo):
        sender="tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"
        params = """Vote( 1n )"""
        storage = """
            record
                proposal = ( "great feature" : hash );
                voters = (map ("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" : address ) -> 10n end : voters);
                votes = (map 1n -> 0n; 2n -> 0n; 3n -> 0n; end : map(nat,nat));
                enddate = ( now - 1: timestamp);
                admins = ( set ("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" : address) end : set(address) )
            end"""
        out = ligo.run_function(
                [ "--sender", sender,
                  contract, "main", f"( {params}, {storage} )"])
        assert out['status'] == "error"

    def test_init(self, ligo):
        sender="tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"
        params = """Init( "A different great feature" )"""
        storage = """
            record
                proposal = ( "great feature" : hash );
                voters = (map ("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" : address ) -> 10n end : voters);
                votes = (map 1n -> 0n; 2n -> 0n; 3n -> 0n; end : map(nat,nat));
                enddate = ( now - 10 : timestamp);
                admins = ( set ("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" : address) end : set(address) )
            end"""
        out = ligo.run_function(
                [ "--sender", sender,
                  contract, "main", f"( {params}, {storage} )"])
        assert out['content'] == "( [] , {votes = [ +1 -> +0 ; +2 -> +0 ; +3 -> +0 ] , voters = [] , proposal = \"A different great feature\" , enddate = +-9 , admins = { \n  address \"tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx\" }} )\n"

    def test_startvote(self, ligo):
        sender="tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"
        params = """StartVote(1577993515)"""
        storage = """
            record
                proposal = ( "great feature" : hash );
                voters = (map ("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" : address ) -> 10n end : voters);
                votes = (map 1n -> 0n; 2n -> 0n; 3n -> 0n; end : map(nat,nat));
                enddate = ( now : timestamp );
                admins = ( set ("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" : address) end : set(address) )
            end"""
        out = ligo.run_function(
                [ "--sender", sender,
                  contract, "main", f"( {params}, {storage} )"])
        assert out['content'] == "( [] , {votes = [ +1 -> +0 ; +2 -> +0 ; +3 -> +0 ] , voters = [ address \"tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx\" -> +10 ] , proposal = \"great feature\" , enddate = +1577993516 , admins = { \n  address \"tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx\" }} )\n"

    def test_startvote_if_ongoing(self, ligo):
        sender="tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"
        params = """StartVote(now)"""
        storage = """
            record
                proposal = ( "great feature" : hash );
                voters = (map ("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" : address ) -> 10n end : voters);
                votes = (map 1n -> 0n; 2n -> 0n; 3n -> 0n; end : map(nat,nat));
                enddate = ( now + 10 : timestamp);
                admins = ( set ("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" : address) end : set(address) )
            end"""
        out = ligo.run_function(
                [ "--sender", sender,
                  contract, "main", f"( {params}, {storage} )"])
        assert out['status'] == "error"
