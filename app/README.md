# Tezos Dapps example

The Tezos platform can be used to deploy smart contract. Those are written in the
smart contract language *Michelson* . This _distributed application_ (dapp) is written
in ReasonML[1] and the ReasonReact[2] stack. It is a simple example on how to write
a web application to interact with a smart contract deployed on the Tezos network.

# Development

It is recommended to use this with a babylonnet sandbox.

    yarn start

# Deployment

    yarn build

    yarn watch

# Testing

To build and test the application
    yarn test

To watch changes during app and test development
    yarn test:watch