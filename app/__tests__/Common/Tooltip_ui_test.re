[@bs.config {jsx: 3}];

open Jest;
open Expect;
open JestDom;
open ReactTestingLibrary;

/**TODO: test mouseover event with bs-webapi lib https://testing-library.com/docs/bs-react-testing-library/examples */

describe("Tooltip UI", () => {

  let element = (
        ReasonReact.cloneElement(
          <Tooltip tooltip_text="A tooltip">
            <div><h1>{"triggers my tooltip!" |> ReasonReact.string}</h1></div>
            </Tooltip> ,
          ~props={"data-testid": "tooltip_id"},
          [||]
        )
  );

  test("Tooltip renders props text", () => 
    element
    |> render
    |> getByText(~matcher=`Str("A tooltip"))
    |> expect
    |> toBeInTheDocument
  )
})