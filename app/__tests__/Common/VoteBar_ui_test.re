[@bs.config {jsx: 3}];

open Jest;
open Expect;
open JestDom;
open ReactTestingLibrary;

describe("VoteBar UI", () => {

  let element = (
        ReasonReact.cloneElement(
          <VoteBar percent={BigNumber.make("12")} name="A Bar" /> ,
          ~props={"data-testid": "vote-bar"},
          [||]
        )
  );

  test("VoteBar renders label", () => 
    element
    |> render
    |> getByText(~matcher=`Str("A Bar"))
    |> expect
    |> toBeInTheDocument
  )

  test("VoteBar uses percent prop properly for sr", () => 
    element
    |> render
    |> getByTitle("Value: 12%")
    |> expect
    |> toBeInTheDocument
  )

  test("VoteBar uses percent prop properly for render width", () => 
    element
    |> render
    |> getByTitle("Value: 12%")
    |> expect
    |> toHaveStyle("width: 12%")
  )

  test("VoteBar uses green when no color prop passed", () =>
    element
      |> render
      |> getByTitle("Value: 12%")
      |> expect
      |> toHaveStyle("background-color: green")
  )

  test("VoteBar uses prop color when color prop passed", () =>
    <VoteBar percent={BigNumber.make("12")} name="A Bar" bar_color="#FFFFFF" />
      |> render
      |> getByTitle("Value: 12%")
      |> expect
      |> toHaveStyle("background-color: #FFFFFF")
  )
})