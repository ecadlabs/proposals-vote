[@bs.config {jsx: 3}];

open Jest;
open Expect;
open JestDom;
open ReactTestingLibrary;

/**TODO: test click event with bs-webapi lib https://testing-library.com/docs/bs-react-testing-library/examples */

describe("Switch UI", () => {
  
  let element = (el) => (
    ReasonReact.cloneElement(
      el,
      ~props={"data-testid": "switch_id"},
      [||]
    )
  );

  test("Switch not checked when value false", () => 
    element(<Switch value={false} onChange={ignore}/>)
    |> render
    |> getByAltText("switch")
    |> expect
    |> Expect.not_
    |> toHaveAttribute("checked")
  )

  test("Switch checked when value true", () => 
    element(<Switch value={true} onChange={ignore}/>)
    |> render
    |> getByAltText("switch")
    |> expect
    |> toHaveAttribute("checked")
  )

  test("Switch has left labels when provided", () => 
    element(<Switch value={true} onChange={ignore} leftLabel="Left" rightLabel="Right"/>)
    |> render
    |> getByText(~matcher=`Str("Left"))
    |> expect
    |> toBeInTheDocument
  )

  test("Switch has right labels when provided", () => 
    element(<Switch value={true} onChange={ignore} leftLabel="Left" rightLabel="Right"/>)
    |> render
    |> getByText(~matcher=`Str("Right"))
    |> expect
    |> toBeInTheDocument
  )

})