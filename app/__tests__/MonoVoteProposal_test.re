open Jest;
open Expect;
open MonoVoteProposal;
let bigZero = BigNumber.make("0");

describe("Init empty vote", () => {

  test("empty vote abstain initialized to zero", () => {
    expect(Vote.empty().abstain) |> toEqual(bigZero)
  })
  test("empty vote pass initialized to zero", () => {
    expect(Vote.empty().pass) |> toEqual(bigZero)
  })
  test("empty vote yay initialized to zero", () => {
    expect(Vote.empty().yay) |> toEqual(bigZero)
  })
  test("empty vote nay initialized to zero", () => {
    expect(Vote.empty().nay) |> toEqual(bigZero)
  })
})


describe("to_percentage", () => {
  test("to_percentage calculates percentages based on weight", () => {
    let mockResults = {
        Vote.abstain: BigNumber.make("12"),
        yay: BigNumber.make("100"),
        nay: BigNumber.make("3"),
        pass: BigNumber.make("0"),
      };

    let expectedPercents = {
        Vote.abstain: BigNumber.make("10.43"),
        yay: BigNumber.make("86.96"),
        nay: BigNumber.make("2.61"),
        pass: BigNumber.make("0"),
      };

    expect(Vote.to_percentage(mockResults, BigNumber.make("115"))) |> toEqual(expectedPercents);
  })

  /* TODO: would like to handle when the total_weight is 0, but BigNumber comparisons are not
  correctly typed for ReasonML it seems?
  test("to_percentages returns 0 for all options if total_weight is zero", () => {})
  */

});

describe("vote map", () => {
  test("vote map applies f to each key", () => {
    let mockResults = {
        Vote.abstain: BigNumber.make("12"),
        yay: BigNumber.make("100"),
        nay: BigNumber.make("3"),
        pass: BigNumber.make("0"),
      };

    let expected = {
        Vote.abstain: BigNumber.make("120"),
        yay: BigNumber.make("1000"),
        nay: BigNumber.make("30"),
        pass: BigNumber.make("0"),
    }

    expect(Vote.map(mockResults, v => BigNumber.multiplied_by_int(v, 10))) |> toEqual(expected)
  })
})

describe("vote fold", () => {
  test("vote fold applies f to each key", () => {
    let mockResults = {
        Vote.abstain: BigNumber.make("12"),
        yay: BigNumber.make("100"),
        nay: BigNumber.make("3"),
        pass: BigNumber.make("0"),
      };

    let expected = BigNumber.make("115")
    expect(Vote.fold(mockResults, BigNumber.of_int(0), BigNumber.plus_bignum)) |> toEqual(expected)
  })

  test("vote fold applies f to each key with initial accum ", () => {
    let mockResults = {
        Vote.abstain: BigNumber.make("12"),
        yay: BigNumber.make("100"),
        nay: BigNumber.make("3"),
        pass: BigNumber.make("0"),
      };

    let expected = BigNumber.make("135")
    expect(Vote.fold(mockResults, BigNumber.of_int(20), BigNumber.plus_bignum)) |> toEqual(expected)
  })
})




