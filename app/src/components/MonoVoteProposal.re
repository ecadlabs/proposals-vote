[@bs.config {jsx: 3}];

open PromiseMonad;
open MonoVoteStorage.T;

module Styles = {
  open Css;

  let container =
    style([
      display(flexBox),
      padding4(~top=px(0), ~bottom=px(45), ~left=px(45), ~right=px(55)),
      alignItems(center),
    ]);

  let proposalCard =
    style([
      margin(`auto),
      maxWidth(`rem(60.)),
      padding(`em(3.)),
      borderRadius(`px(5)),
    ]);
};

module Vote = {
  type t = {
    abstain: BigNumber.t,
    yay: BigNumber.t,
    nay: BigNumber.t,
    pass: BigNumber.t,
  };

  let empty = () => {
    abstain: BigNumber.make("0"),
    yay: BigNumber.make("0"),
    nay: BigNumber.make("0"),
    pass: BigNumber.make("0"),
  };

  let map = ({abstain, yay, nay, pass}, f) => {
    {abstain: f(abstain), yay: f(yay), nay: f(nay), pass: f(pass)};
  };

  let fold = ({abstain, yay, nay, pass}, acc, f) => {
    acc |> f(abstain) |> f(yay) |> f(nay) |> f(pass);
  };

  let map_trans = ({abstain, yay, nay, pass}, f) => {
    (f(abstain), f(yay), f(nay), f(pass));
  };

  let from_storage = (storage: MonoVoteStorage.T.t) => {
    let abstain =
      Js.Dict.values(storage.voters)
      |> Array.fold_left(BigNumber.plus_bignum, BigNumber.of_int(0));
    {abstain, yay: storage.yay, nay: storage.nay, pass: storage.pass};
  };

  // debug
  let to_string = (~decorator="", {abstain, yay, nay, pass}) =>
    "abstain: "
    ++ abstain->BigNumber.to_string
    ++ decorator
    ++ "\nyay: "
    ++ yay->BigNumber.to_string
    ++ decorator
    ++ "\nnay: "
    ++ nay->BigNumber.to_string
    ++ decorator
    ++ "\npass: "
    ++ pass->BigNumber.to_string
    ++ decorator;

  let to_vote_table = ({abstain, yay, nay, pass}) =>
    <table
      style={ReactDOMRe.Style.make(~width="95%", ~tableLayout="fixed", ())}>
      <tbody>
        <VoteBar percent=abstain name="Not Yet Voted" bar_color="gray" />
        <VoteBar percent=yay name="Yay" bar_color="#2ECA9B" />
        <VoteBar percent=nay name="Nay" bar_color="#FD2D49" />
        <VoteBar percent=pass name="Pass" bar_color="black" />
      </tbody>
    </table>;

  let to_percentage = (t, total_weight: BigNumber.t) => {
    let bignum_to_percent = t => {
      BigNumber.divided_by(t, total_weight)
      ->BigNumber.multiplied_by_int(100)
      ->BigNumber.decimal_places(2);
    };
    map(t, bignum_to_percent);
  };
};

[@react.component]
let make = (~addr, ~signer, ~host="http://localhost:18731") => {

  Taquito.tezos->Taquito.setProvider({rpc: host, signer});
  
  let (results, set_results) = React.useState(() => None);

  let get_enddate =
    fun
    | None => "Unknown"
    | Some((res: MonoVoteStorage.T.t)) => {
        let expires = Js.Date.fromString(res.enddate);
        if (Js.Date.now() > Js.Date.valueOf(expires)) {
          "Vote Completed on "
          ++ Js.Date.toString(expires)->String.sub(0, 24)
          ++ ".";
                // ++ Js.Date.toString(Js.Date.fromFloat(Js.Date.now()));
        } else {
          let timeleft = Js.Date.valueOf(expires) -. Js.Date.now();
          Js.Date.toString(Js.Date.fromFloat(Js.Date.now() +. timeleft));
        };
      };

  let get_proposal =
    fun
    | None => "Unknown"
    | Some((res: MonoVoteStorage.T.t)) => res.proposal;

  let show_results =
    fun
    | None => "There is no results to show yet"->ReasonReact.string
    | Some((res: MonoVoteStorage.T.t)) => {
        let vote = Vote.from_storage(res);
        let total_weight =
          Vote.fold(vote, BigNumber.of_int(0), BigNumber.plus_bignum);
        let vote = Vote.to_percentage(vote, total_weight);
        Vote.to_vote_table(vote);
      };

  let monoVoteOptions: array(VoteTypes.Option.t) = [|
      {name: "Yay", value: 1},
      {name: "Nay", value: 2}, 
      {name: "Pass", value: 3}
  |];

  let showVoteForm =
    fun
    | None => React.null
    | Some((res: MonoVoteStorage.T.t)) => {
      let expires = Js.Date.fromString(res.enddate);
      Js.Date.now() > Js.Date.valueOf(expires) 
        ? React.null
        : <VoteForm addr host signer options=monoVoteOptions/>
    };

  let fetch_results = _ =>
    Taquito.tezos->MonoVoteStorage.get_storage(addr)
    >>- (storage => set_results(_ => Some(storage)))
    |> ignore;

  /* Fetch data on load */
  React.useEffect1(
    () => {
      fetch_results();
      Js.Global.setInterval(fetch_results, 5000)->ignore;
      None;
    },
    [||],
  );

  <div>
    <div className=Styles.proposalCard>
      
      <h2> {"Proposal: " ++ get_proposal(results) |> ReasonReact.string} </h2>
      <div> {get_enddate(results)->ReasonReact.string} </div>
      <h3> "Results"->ReasonReact.string </h3>
      <div> {show_results(results)} </div>
      {showVoteForm(results)}
    </div> 
  </div>;
};