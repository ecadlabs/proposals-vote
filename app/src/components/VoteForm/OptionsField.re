[@bs.config {jsx: 3}];

module Styles = {
  open Css;

  let optionContainer = 
  style([
    display(`flex),
    flexDirection(`row),
    flexWrap(`wrap),
    justifyContent(`spaceEvenly),
    selector("button", [
      flexGrow(0.3)
    ])
  ])

let selected =
  style([
    border(`px(2), `solid, `hex("2196F3")),
    transform(scale(1.13, 1.15))
  ]);

};

[@react.component]
let make = (~options, ~doVote, ~choice=0,) => {
    let makeOption = (opt: VoteTypes.Option.t) => (
        <button 
          key={opt.name} 
          className={opt.value == choice ? Styles.selected : ""} 
          onClick={_ => doVote(opt.value)}>
          opt.name -> ReasonReact.string
        </button>
      );
    
      let makeOptions = (): array(ReasonReact.reactElement) => Array.map(makeOption, options);
    
    <div>
      <label>{"Options" -> ReasonReact.string}</label>
      <div className={Styles.optionContainer}>
        (ReasonReact.array(makeOptions()))
      </div>
    </div>
};