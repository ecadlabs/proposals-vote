module Option {
    type t = {
      name: string,
      value: int
    }
  }
  
type voteState = {
voter: string,
error: option(string),
voteInBrowser: bool,
choice: option(int),
};