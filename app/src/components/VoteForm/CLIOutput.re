[@bs.config {jsx: 3}];

module Styles = {
  open Css;

  let textContainer = style([
    display(`flex),
    flexDirection(`row),

  ])

  let copyButton = style([
    border(`px(0), `solid, `transparent),
    boxShadow(`unset),
    padding(`rem(0.0)),
    paddingLeft(`rem(0.25)),
    margin(`px(0)),
    marginLeft(`rem(0.5)),
    outlineWidth(`px(0))
  ])

  let clientText = style([
    flexGrow(1.)
  ])
};

[@react.component]
let make = (~addr="", ~voter="", ~choice=0, ~show) => {

  let (copySuccess, setCopySuccess) = React.useState(_ => false);

  let cliText = () => 
    choice > 0
    ? "tezos-client transfer 0 from " 
      ++ voter 
      ++ " to " 
      ++ addr 
      ++ " --arg '"
      ++ string_of_int(choice)
      ++ "' --entrypoint \"vote\""
    : ""
  
  
  let copyText = () => {
    [%bs.raw {| document.getElementById("client-text").select() |}];
    [%bs.raw {| document.execCommand('copy') |}];
    setCopySuccess(_ => true);
    Js.Global.setTimeout(_ => setCopySuccess(_ => false), 2000)->ignore;
  };

  show
    ? <div>
        <label htmlFor="client-text">{"CLI Command"->ReasonReact.string}
          <aside>{"Copy this text into your CLI tool using the tezos client to vote from your local machine." -> ReasonReact.string}</aside>
        </label>
        <div className={Styles.textContainer}>
          <textarea 
            className={Styles.clientText}
            readOnly={true}
            rows={3}
            type_="text" 
            id="client-text" 
            value={cliText()}/>
          <button disabled={choice == 0} onClick={_ => copyText()} className={Styles.copyButton}>
            {Icon.getIcon(~height="1.5rem", ~fill=(copySuccess ? "#2196F3" : "#333333"), ~iconName="copy")}
          </button>
        </div>
      </div>
    : React.null;
};
