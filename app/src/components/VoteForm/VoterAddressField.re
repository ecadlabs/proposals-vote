[@bs.config {jsx: 3}];


[@react.component]
let make = (~voter, ~onChange) => {

    // TODO: validate address

    let onFocus = event => ReactEvent.Focus.target(event)##select();

    <div>
      <label htmlFor="voter">
        {"Voter Address" |> ReasonReact.string}
      </label>
      <input
        type_="text"
        name="voter"
        onFocus
        onChange={event => 
          onChange(ReactEvent.Form.target(event)##value)
        }
        value=voter
      />
    </div>
};



