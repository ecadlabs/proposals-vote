module T = {
  type t = {
    proposal: string,
    enddate: string,
    voters: Js.Dict.t(BigNumber.t),
    yay: BigNumber.t,
    nay: BigNumber.t,
    pass: BigNumber.t,
  };

  let cast = a => {
    open Json.Decode;
    let to_bignum_trust_me: _ => BigNumber.t = t => [%bs.raw {|t|}];
    {
      proposal: a |> field("proposal", string),
      enddate: a |> field("enddate", string),
      yay: a |> field("votes", field("1", to_bignum_trust_me)),
      nay: a |> field("votes", json => field("2", to_bignum_trust_me, json)),
      pass: a |> field("votes", json => field("3", to_bignum_trust_me, json)),
      voters: a |> field("voters", dict(to_bignum_trust_me)),
    };
  };
};

include Taquito.ContractProvider(T);
