[@bs.config {jsx: 3}];

module Styles = {
  open Css;

  let tooltipContainer = style([
    position(`relative),
    selector(".tooltip-text", [
      visibility(`hidden),
      width(`px(120)),
      backgroundColor(black),
      color(rgb(255, 255, 255)),
      textAlign(`center),
      padding(`px(5)),
      borderRadius(`px(6)),
      position(`absolute),
      zIndex(1),
    ]),
    selector(":hover .tooltip-text", [visibility(`visible),])
  ]);

};

[@react.component]
let make = (~tooltip_text, ~children) => {
  <div className=Styles.tooltipContainer>
    <span role="tooltip" className="tooltip-text">
      tooltip_text->ReasonReact.string
    </span>
    children
  </div>;
};