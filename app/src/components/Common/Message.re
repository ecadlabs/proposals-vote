[@bs.config {jsx: 3}];

module Styles = {
  open Css;

  let messageContainer = style([
    position(`relative),
    width(`percent(80.0)),
    margin2(~v=`px(10), ~h=`auto),
    border(`px(2), `solid, rgb(235, 52, 52)),
    borderRadius(`px(5)),
    padding(`px(5))
  ]);

  let closeButton = style([
    boxShadow(`none),
    border(`px(0), `none, rgb(0,0,0)),
    padding(`px(2)),
    position(`absolute),
    right(`px(2)),
    top(`px(2)),
  ])

  let messageTitle = style([marginBottom(`rem(0.5))])
};

[@react.component]
let make = (~message=?, ~title="An Error Occurred", ~onClose) => {
  switch (message) {
    | None => React.null
    | Some(messageStr) => (
      <div className={Styles.messageContainer}>
        <button className={Styles.closeButton} onClick={_ => onClose()}>
          {Icon.getIcon("close")}
        </button>
        <div>
          <h5 className={Styles.messageTitle}>{title->ReasonReact.string}</h5>
          <p>{messageStr->ReasonReact.string}</p>
        </div>
      </div>
      );
    } 
};