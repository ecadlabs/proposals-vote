[@bs.config {jsx: 3}];

module Styles = {
  open Css;

  let switchContainer = style([
    display(`flex),
    flexDirection(`row),
    alignItems(`center)
  ]);

  let optLabel = style([
    important(fontSize(`rem(0.85)))
  ]);

  let switch_ = style([
    position(`relative),
    display(`inlineBlock),
    margin2(~v=`rem(0.25), ~h=`rem(0.5)),
    width(`px(50)),
    height(`px(24))
  ]);

  let switchInput = style([
    opacity(0.0),
    width(`px(0)),
    height(`px(0)),
    selector(":checked + span", [
      backgroundColor(`hex("2196F3"))
    ]),
    selector(":focus + span", [
      boxShadow(Shadow.box(~x=`px(0), ~y=`px(0), ~blur=`px(1), `hex("2196F3")))
    ]),
    selector(":checked + span:before", [
      transform(translateX(`px(26)))
    ])
  ]);
  
  let slider = style([ 
    position(`absolute),
    cursor(`pointer),
    top(`px(0)),
    left(`px(0)),
    right(`px(0)),
    bottom(`px(0)),
    
    transitionDuration(400),
    borderRadius(`px(24)),
    selector(":before", [
      position(`absolute),
      contentRule(""),
      height(`px(16)),
      width(`px(16)),
      left(`px(4)),
      bottom(`px(4)),
      backgroundColor(`hex("fff")),
      transitionDuration(400),
      borderRadius(`percent(50.))
    ])
  ]);

  let onOffSlider = style([
    backgroundColor(`hex("ccc"))
  ]);

  let dualChoiceSlider = style([
    backgroundColor(hex("2196F3"))
  ]);
};

[@react.component]
let make = (~value, ~onChange, ~leftLabel="", ~rightLabel="", ~isOnOff=true) => {
  <div className={Styles.switchContainer}>
  {
    leftLabel != "" 
      ? <label className={Styles.optLabel}>{leftLabel -> ReasonReact.string}</label> 
      : React.null}
  <label className={Styles.switch_}>
    <input alt="switch" className={Styles.switchInput} checked={value} type_="checkbox" onChange={(evt) => onChange(ReactEvent.Form.target(evt)##checked)}/>
    <span role="slider" className={Styles.slider ++ " " ++ (isOnOff ? Styles.onOffSlider : Styles.dualChoiceSlider)}></span>
  </label>
  {
    rightLabel != "" 
      ? <label className={Styles.optLabel}>{rightLabel -> ReasonReact.string}</label> 
      : React.null
  }
  </div>;
};