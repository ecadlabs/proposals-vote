let upload_file =
    /* We can't do this in a typesafe way cause FileReader is unimplemented in
       bs-webapi */
    [%bs.raw
      {|
    function (id, callback) {
    let file = document.getElementById(id).files[0]
    var reader = new FileReader();
    reader.readAsText(file, "UTF-8");
    reader.onload = function (evt) {
        console.log("ok!")
        callback(evt.target.result)
        console.log("ok")
    };
    reader.onerror = function (evt) {
      console.log("error")
    }
    }
  |}
    ];
