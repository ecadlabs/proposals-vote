[@bs.config {jsx: 3}];
[@bs.module] external logo: string = "./logo.svg";

// left here for debug
let example = "KT1W4mTnxNsmvRL5waonTMGWmB4wnZ7vRm6R";

let eventValue = evt => evt->ReactEvent.Form.target##value;

module Styles = {
  open Css;
  global("button, input, textarea", [
    border(`px(1), `solid, rgb(200, 200, 200)),
    boxShadow(Shadow.box(~x=`px(1), ~y=`px(1), ~blur=`px(2), rgb(240,240,240))),
  ]);

  global("button", [
    selector(":hover", [cursor(`pointer), transform(scale(1.1,1.1))]),
    padding(`rem(0.5)),
    borderRadius(`px(8)),
    backgroundColor(white),
    margin(`em(0.5)),
    selector(":disabled", [
      opacity(0.5),
      selector(":hover", [cursor(`auto), transform(scale(1., 1.))])
    ])
  ]);

  global("input, textarea", [
    padding(`px(5)),
    borderRadius(`px(5)),
    margin2(~v=`px(2), ~h=`px(0)),
  ]);

  let app = style([textAlign(`center)]);
  let appLogo =
    style([
      //animation()
      height(`vmin(40.)),
    ]);
  let appHeader =
    style([
      backgroundColor(`hex("282c34")),
      minHeight(`vh(50.)),
      display(`flex),
      flexDirection(`column),
      alignItems(`center),
      justifyContent(`center),
      fontSize(`calc((`add, `px(10), `vmin(2.)))),
      color(white),
    ]);
  let appLink = style([color(`hex("61daf"))]);
};

module ContractList = {
  [@react.component]
  let make = () => {
    let (contractlist, addContract) = React.useState(() => [example]);

    let (termInput, setInput) = React.useState(() => "");
    let handle_onKeyDown = event => {
      let key = ReactEvent.Keyboard.key(event);
      if (key == "Enter") {
        addContract(contractlist => [termInput, ...contractlist]);
      };
    };

    <div>
      <h2> {React.string("Add Poll")} </h2>
      <div>
        <label>
          {React.string("Poll Address")}
          <input
            type_="text"
            name="contract"
            onChange={event => setInput(eventValue(event))}
            onKeyDown=handle_onKeyDown
          />
        </label>
      </div>
      <h2> {React.string("Poll List")} </h2>
      <ul>
        {(
           contractlist
           |> List.map(name =>
                <li key=name>
                  <a href=name> {React.string("Vote " ++ name)} </a>
                </li>
              )
         )
         ->Array.of_list
         ->ReasonReact.array}
      </ul>
    </div>;
  };
};

[@react.component]
let make = (~message) => {
  let url = ReasonReactRouter.useUrl();

  <div className=Styles.app>
    <div className=Styles.appHeader>
      <h2> {ReasonReact.string(message)} </h2>
    </div>
    <div>
      {switch (url.path) {
       | [addr] =>
         <MonoVoteProposal addr signer={Taquito.new_tezbridge_signer()} />
       | _ => <ContractList />
       }}
    </div>
  </div>;
};