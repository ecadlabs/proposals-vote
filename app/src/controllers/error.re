type error =
  | VoteEnded
  | UnregisteredVoter
  | Unknown;

let to_string = fun
  | VoteEnded => "The vote has ended."
  | UnregisteredVoter => "This address is not allowed to vote."
  | Unknown => "An error occured."

external to_json: Js.Promise.error => Js.Json.t = "%identity";
let parse = err => {
  open Json.Decode;
  // TODO this is bad
  // Thing is, I don't know how to differentiate between a contract error and an error 500
  // if the signature is wrong.
  let msg = [%bs.raw {| err.message || "{ \"errors\": [{\"with\": {\"string\": \"Unkown error\"}}]}" |}];
  Js.log(msg)
  let err = Json.parseOrRaise(msg);
  Js.log(err);
  let reason = optional(field("with", field("string", string)));
  let errors = err |> field("errors", array(reason));
  let reason =
    errors
    |> Array.to_list
    |> List.filter(
         fun
         | None => false
         | _ => true,
       )
    |> List.map(
         fun
         | None => failwith("")
         | Some(v) => v,
       )
    |> List.hd;
  switch (reason) {
  | "The vote has ended." => VoteEnded
  | _ => Unknown
  };
};