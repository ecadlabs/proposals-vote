#/bin/bash
tfile=$(mktemp /tmp/foo.XXXXXXXXX)

tezos-mainnet.client -A teznode.letzbake.com  -P 443 --tls rpc get chains/main/blocks/head/votes/listings > $tfile

n=$(cat /tmp/tnnn | jq -c  '. | length')
incr=250
s=0
e=$incr
for i in `seq -s ' ' 0 $(($n / $incr))`; do
  cat $tfile | jq -c  ".[$s:$e]" > voters$i.json
  s=$e
  e=$(($s + $incr))
done
