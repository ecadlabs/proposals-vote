#!/usr/bin/python
import argparse
import os
import json
from tezos_launchers.client.ligo import Ligo
from tezos_launchers.client.ligo import format_command

cur_path = os.path.dirname(os.path.realpath(__file__))
path = os.path.dirname(cur_path) + '/'
contract = os.path.join(path,"src/contracts/", "mono_vote.ligo")

def voters(filename):
    data = json.load(open(filename,"r"))
    out = ""
    for r in data:
        out += f"(\"{r['pkh']}\" : address ) -> {r['rolls']}n;";
    print(out)
    return out

def gen_params_addVoters(filename):
    params = f"AddVoters( (map {voters(filename)} end : map(address,nat)) )"
    ligo = Ligo(pwd=path)
    out = ligo.run(
            [ "compile-parameter", "--format", "json",
                contract, "main", f"{params}"])
    return json.loads(out)["content"]

def gen_params_startVote(duration):
    params = f"StartVote( ({duration} : int ) )"
    ligo = Ligo(pwd=path)
    out = ligo.run(
            [ "compile-parameter", "--format", "json",
                contract, "main", f"{params}"])
    return json.loads(out)["content"]

def gen_params_init(filename):
    params = f"Init: (map {voters(filename)} end : map(address,nat)))"
    ligo = Ligo(pwd=path)
    out = ligo.run(
            [ "compile-parameter", "--format", "json",
                contract, "main", f"{params}"])
    return json.loads(out)["content"]


def main():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('-v', '--verbose')
    parser.add_argument('--jsonfile', type=str, nargs=1, help="json file")
    parser.add_argument('--addr', type=str, nargs=1, help="contract KT1")
    parser.add_argument('--client', type=str, nargs=1, default=["/snap/bin/tezos-mainnet.client"])
    parser.add_argument('--duration', type=int, nargs=1, help="vote duration from now", default=1)

    args = parser.parse_args()

    fname = args.jsonfile[0]
    client = args.client[0]
    duration = args.duration[0]

    cmd = [
            client,
            "transfer", "0",
            "from", "bootstrap1",
            "to", args.addr[0],
            "--arg", f"'{gen_params_addVoters(fname)}'",
            "--burn-cap", "0.124"
            ]
    formatted_cmd = format_command(cmd)
    print("\n", formatted_cmd)

    print(gen_params_startVote(duration))
    cmd = [
            client,
            "transfer", "0",
            "from", "bootstrap1",
            "to", args.addr[0],
            "--arg", f"'{gen_params_startVote(duration)}'",
            "--burn-cap", "0.124"
            ]
    formatted_cmd = format_command(cmd)
    print("\n", formatted_cmd)

if __name__ == '__main__':
    main()

